﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;

namespace UIApp
{
    /// <summary>
    /// Структура с эталонными изображениями в формате, готовом для использования в OpenCV.
    /// Содержит изображение в формате Mat и его название.
    /// </summary>
    struct ModelImage
    {
        public string Name;
        public Bitmap Icon;


        public List<Mat> Images;

        public ModelImage(string name, Bitmap image, Bitmap[] hiddenModelImages)
        {
            Name = name;
            Icon = image;

            Images = new List<Mat>(
                hiddenModelImages.Select(img => new Image<Rgba, Byte>(img).Mat)
                );
        }
    }

    static class ModelImages
    {
        /// <summary>
        /// Список эталонных изображений
        /// </summary>
        public static List<ModelImage> Items = new List<ModelImage>();

        static ModelImages()
        {
            var tuples = new(string Name, Bitmap BitmapImage, Bitmap[] hiddenModelImages)[]
            {
                ("Железнодорожный переезд без шлагбаума", Properties.Resources._1_2, new Bitmap[] {
                    Properties.Resources._1_2_1,
                    Properties.Resources._1_2_10,
                    Properties.Resources._1_2_11,
                    Properties.Resources._1_2_12,
                    Properties.Resources._1_2_13,
                    Properties.Resources._1_2_14,
                    Properties.Resources._1_2_15,
                    Properties.Resources._1_2_16,
                    Properties.Resources._1_2_17,
                    Properties.Resources._1_2_18,
                    Properties.Resources._1_2_19,
                    Properties.Resources._1_2_2,
                    Properties.Resources._1_2_20,
                    Properties.Resources._1_2_21,
                    Properties.Resources._1_2_22,
                    Properties.Resources._1_2_23,
                    Properties.Resources._1_2_3,
                    Properties.Resources._1_2_4,
                    Properties.Resources._1_2_5,
                    Properties.Resources._1_2_6,
                    Properties.Resources._1_2_7,
                    Properties.Resources._1_2_8,
                    Properties.Resources._1_2_9
                }),

                ("Пересечение с круговым движением", Properties.Resources._1_7, new Bitmap[] {
                    Properties.Resources._1_7_1,
                    Properties.Resources._1_7_2,
                    Properties.Resources._1_7_3,
                    Properties.Resources._1_7_4,
                    Properties.Resources._1_7_5
                }),

                ("Пешеходный переход", Properties.Resources._1_22, new Bitmap[] {
                    Properties.Resources._1_22_1,
                    Properties.Resources._1_22_2,
                    Properties.Resources._1_22_3,
                    Properties.Resources._1_22_4,
                    Properties.Resources._1_22_5,
                    Properties.Resources._1_22_6,
                    Properties.Resources._1_22_7,
                    Properties.Resources._1_22_8
                }),

                ("Дети", Properties.Resources._1_23, new Bitmap[] {
                    Properties.Resources._1_23_1,
                    Properties.Resources._1_23_10,
                    Properties.Resources._1_23_11,
                    Properties.Resources._1_23_12,
                    Properties.Resources._1_23_13,
                    Properties.Resources._1_23_14,
                    Properties.Resources._1_23_15,
                    Properties.Resources._1_23_16,
                    Properties.Resources._1_23_17,
                    Properties.Resources._1_23_18,
                    Properties.Resources._1_23_19,
                    Properties.Resources._1_23_2,
                    Properties.Resources._1_23_20,
                    Properties.Resources._1_23_21,
                    Properties.Resources._1_23_22,
                    Properties.Resources._1_23_23,
                    Properties.Resources._1_23_24,
                    Properties.Resources._1_23_25,
                    Properties.Resources._1_23_26,
                    Properties.Resources._1_23_27,
                    Properties.Resources._1_23_28,
                    Properties.Resources._1_23_29,
                    Properties.Resources._1_23_3,
                    Properties.Resources._1_23_30,
                    Properties.Resources._1_23_31,
                    Properties.Resources._1_23_32,
                    Properties.Resources._1_23_33,
                    Properties.Resources._1_23_34,
                    Properties.Resources._1_23_35,
                    Properties.Resources._1_23_36,
                    Properties.Resources._1_23_37,
                    Properties.Resources._1_23_38,
                    Properties.Resources._1_23_39,
                    Properties.Resources._1_23_4,
                    Properties.Resources._1_23_5,
                    Properties.Resources._1_23_6,
                    Properties.Resources._1_23_7,
                    Properties.Resources._1_23_8,
                    Properties.Resources._1_23_9
                }),
            };

            foreach (var tuple in tuples)
            {
                Items.Add(new ModelImage(tuple.Name, tuple.BitmapImage, tuple.hiddenModelImages));
            }
        }
    }
}
