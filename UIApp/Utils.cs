﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.Structure;

namespace UIApp
{
    static class Utils
    {
        public static void DrawQuadToMat(Mat image, PointF[] quad)
        {
            // нарисовать прямоугольник
            Point[] points = Array.ConvertAll<PointF, Point>(quad, Point.Round);
            using (VectorOfPoint vp = new VectorOfPoint(points))
            {
                CvInvoke.Polylines(image, vp, true, new MCvScalar(255, 0, 0, 255), 5);
            }
        }
    }
}
