﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.XFeatures2D;
using Emgu.CV.Structure;
using Emgu.Util;
using System.Drawing;
using Emgu.CV.Util;

namespace UIApp
{
    static partial class Recognizer
    {

        public struct RecognizedItem
        {
            public PointF[] Rect;
            public ModelImage Item;
        }
    }

    static partial class Recognizer
    {
        /// <summary>
        /// Сравнивает данное изображение с эталонами и возвращает совпавшие названия.
        /// Распознавание осуществляется на основе методов KAZE и FLANN.
        /// </summary>
        /// <param name="image">Путь к файлу изображения</param>
        /// <returns></returns>
        public static Task<List<RecognizedItem>> Match(Mat image)
        {
            return new Task<List<RecognizedItem>>(() => MatchSync(image));
        }

        private static List<RecognizedItem> MatchSync(Mat image)
        {
            var results = new List<RecognizedItem>();

            UMat uObservedImage = image.GetUMat(AccessType.Read);
            VectorOfKeyPoint observedKeyPoints = new VectorOfKeyPoint();
            Mat observedDescriptors = new Mat();

            findFeaturesOnObservedImage(uObservedImage, out observedKeyPoints, out observedDescriptors);

            foreach (var modelItem in ModelImages.Items)
            {
                long time;
                foreach (var hiddenModelImage in modelItem.Images)
                {
                    var quad = FeatureMatcher.GetQuad(hiddenModelImage, observedKeyPoints, observedDescriptors, out time);

                    if (validateQuad(quad))
                    {
                        var item = new RecognizedItem();
                        item.Rect = QuadGetsRect(quad);
                        item.Item = modelItem;

                        results.Add(item);

                        break;
                    }
                }
            }

            return results;
        }

        private static void findFeaturesOnObservedImage(
            UMat uObservedImage,
            out VectorOfKeyPoint observedKeyPoints,
            out Mat observedDescriptors)
        {
            KAZE featureDetector = new KAZE();

            observedKeyPoints = new VectorOfKeyPoint();
            observedDescriptors = new Mat();

            featureDetector.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
        }

        private static bool validateQuad(PointF[] quad)
        {
            if (quad == null) return false;

            // мб кидать исключение?
            if (quad.Count() != 4)
                throw new Exception($"Disfigured quad with {quad.Count()} vertices!");

            // грязные подсчеты ручками!

            // проверка на выпуклость
            Func<PointF, PointF, PointF, int> crossCheck = (p1, p2, p3) =>
            {
                var x1 = p2.X - p1.X;
                var y1 = p2.Y - p1.Y;
                var x2 = p3.X - p2.X;
                var y2 = p3.Y - p2.Y;

                var cross = x1 * y2 - x2 * y1;

                var result = Math.Sign(cross);
                if (result == 0) result = 1;

                return result;
            };

            var c1 = crossCheck(quad[0], quad[1], quad[2]);
            var c2 = crossCheck(quad[1], quad[2], quad[3]);
            var c3 = crossCheck(quad[2], quad[3], quad[0]);
            var c4 = crossCheck(quad[3], quad[0], quad[1]);
            if (c1 != c2 ||
                c2 != c3 ||
                c3 != c4 ||
                c4 != c1)
                return false;

            // проверка соотношения длин диагоналей
            Func<PointF, PointF, double> len = (p1, p2) =>
            {
                return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
            };

            double diag1 = len(quad[0], quad[2]);
            double diag2 = len(quad[1], quad[3]);
            double eps = 0.2;

            var ratio = diag1 / diag2;

            if (Math.Abs(ratio - 1) <= eps)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static PointF[] QuadGetsRect(PointF[] quad)
        {
            var xs = quad.Select(q => q.X);
            var ys = quad.Select(q => q.Y);

            var xmin = xs.Min();
            var xmax = xs.Max();
            var ymin = ys.Min();
            var ymax = ys.Max();

            return new PointF[]
            {
                new PointF(xmin, ymin),
                new PointF(xmin, ymax),
                new PointF(xmax, ymax),
                new PointF(xmax, ymin)
            };
        }
    }
}

