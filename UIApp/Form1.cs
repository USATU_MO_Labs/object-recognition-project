﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;


namespace UIApp
{
    public partial class Form1 : Form
    {
        enum Status { Ready, Working }

        Mat observedImage;

        public Form1()
        {
            InitializeComponent();
        }

        private void btRecognize_Click(object sender, EventArgs e)
        {
            string FileName = tbFilePath.Text ?? null;
            if (FileName == null || !File.Exists(FileName)) return;

            updateStatus(Status.Working);

            dataGridView1.Rows.Clear();

            // распознаем эталонные объекты и получаем их рамочки и эталонные картинки (второе наверное не нужно)
            //var items = await 
            var task = Recognizer.Match(observedImage);
            task.Start();

            task.ContinueWith((completedTask) =>
            {
                var items = completedTask.Result;

                // рисуем рамочки
                foreach (var item in items)
                {
                    Utils.DrawQuadToMat(observedImage, item.Rect);
                    dataGridView1.Invoke(new Action(() =>
                            dataGridView1.Rows.Add(new object[] { item.Item.Name, item.Item.Icon }))
                            );
                }

                // рисуем результат
                ShowImage(observedImage);

                updateStatus(Status.Ready);
            });
        }

        private void ShowImage(Mat image)
        {
            Image<Bgr, Byte> img = new Image<Bgr, Byte>(image.Bitmap);
            imageBox1.Image = img;
        }

        private void btBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string FileName = openFileDialog1.FileName;
                tbFilePath.Text = FileName;
                observedImage = CvInvoke.Imread(FileName, ImreadModes.AnyColor);
                ShowImage(observedImage);
            }
        }

        private void updateStatus(Status status)
        {
            switch (status)
            {
                case Status.Ready:
                    lbStatus.Invoke((MethodInvoker)(() => lbStatus.Text = "Готов"));
                    break;

                case Status.Working:
                    lbStatus.Invoke((MethodInvoker)(() => lbStatus.Text = "Работает"));
                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateStatus(Status.Ready);
        }
    }
}
